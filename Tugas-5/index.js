console.log("SOal 1")

function arrayToObject(arr) {
    for (var i=0; i < arr.length; i++) {
        var thisYear = (new Date()).getFullYear();
        var personArr = arr[i];

        var objPerson = {
            firstName   : personArr[0],
            lastName    : personArr[1],
            Gender      : personArr[2],
        }

        if(!personArr[3] || personArr[3] > thisYear){
            objPerson.Age = "invalid Birth Year"
        }else{
                objPerson.Age = thisYear - personArr[3]
        }
        
        
        var fullName = objPerson.firstName + "  " + objPerson.lastName
        // console.log(arr[1]);
        console.log(`${i + 1} . ${fullName} :`, objPerson)
        // console.log(`${fullName} :`, objPerson)
    }

}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

console.log("Soal 2")

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var output = []
    for(var j = 0; j < arrPenumpang.length; j++){
        var penumpangsekarang = arrPenumpang[j]
        var obj = {
            penumpang : penumpangsekarang[0],
            naik : penumpangsekarang[1],
            tujuan : penumpangsekarang[2],
        }
        var bayar = (rute.indexOf(penumpangsekarang[2]) - rute.indexOf(penumpangsekarang[1])) * 2000
        //(rute.IndexOf(penumpangsekarang[2]) - rute.IndexOf(penumpangsekarang[1]))

        obj.bayar = bayar
        output.push(obj)
    }
    return output
  }

  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
   
   
  console.log(naikAngkot([['Ahmad', 'B', 'F'], ['Sukir', 'C', 'F']]))

  console.log("Soal 3")

  function nilaiTertinggi(siswa) {
    var nilai = {}
    for (var k=0; k < siswa.length; k++){
        var sekarang = siswa[k]
        if (!nilai[sekarang.class]){
            var obj = {
                name : sekarang.name,
                score : sekarang.score,
            }

        }
        var total = obj.siswa + " " + obj.score + obj.class;
        // console.log(sekarang.class)
        // console.log(`${total}`);
        console.log(`${sekarang.class} : `, obj);
    }
  }

  var data = [ [ 'Asep', 90, 'adonis']]

  
  
  // TEST CASE
  console.log(nilaiTertinggi([
    {
      name: 'Asep',
      score: 90,
      class: 'adonis'
    },
    {
      name: 'Ahmad',
      score: 85,
      class: 'vuejs'
    },
    {
      name: 'Regi',
      score: 74,
      class: 'adonis'
    },
    {
      name: 'Afrida',
      score: 78,
      class: 'reactjs'
    }
  ]));
  
  // OUTPUT:
  
  // {
  //   adonis: { name: 'Asep', score: 90 },
  //   vuejs: { name: 'Ahmad', score: 85 },
  //   reactjs: { name: 'Afrida', score: 78}
  // }
  
  
  console.log(nilaiTertinggi([
    {
      name: 'Bondra',
      score: 100,
      class: 'adonis'
    },
    {
      name: 'Putri',
      score: 76,
      class: 'laravel'
    },
    {
      name: 'Iqbal',
      score: 92,
      class: 'adonis'
    },
    {
      name: 'Tyar',
      score: 71,
      class: 'laravel'
    },
    {
      name: 'Hilmy',
      score: 80,
      class: 'vuejs'
    }
  ]));
  
  // {
  //   adonis: { name: 'Bondra', score: 100 },
  //   laravel: { name: 'Putri', score: 76 },
  //   vuejs: { name: 'Hilmy', score: 80 }
  // }