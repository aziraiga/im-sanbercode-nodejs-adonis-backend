"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _kelas = _interopRequireDefault(require("./kelas"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var bootcamp = /*#__PURE__*/function () {
  function bootcamp(nama) {
    _classCallCheck(this, bootcamp);

    this._nama = nama;
    this._student = [];
  }

  _createClass(bootcamp, [{
    key: "nama",
    get: function get() {
      return this._nama;
    },
    set: function set(strnama) {
      this._nama = strnama;
    }
  }, {
    key: "student",
    get: function get() {
      return this._student;
    } // set student(strstudent){
    // this._student = strstudent
    // }   

  }, {
    key: "createKelas",
    value: function createKelas(nama, level, instructor) {
      var newKelas = new _kelas["default"](nama, level, instructor);

      this._student.push(newKelas);
    }
  }, {
    key: "insertStudent",
    value: function insertStudent(nama, objkelas) {
      var carinama = this._nama.find(function (post) {
        return post.nama === nama;
      });

      carinama.addStudents(objkelas);
    }
  }]);

  return bootcamp;
}();

var _default = bootcamp;
exports["default"] = _default;