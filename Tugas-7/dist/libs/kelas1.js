"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var Kelas = /*#__PURE__*/function () {
  function Kelas(nama, level, instructor) {
    _classCallCheck(this, Kelas);

    this._nama = nama;
    this._student = [];
    this._level = level;
    this._instructor = instructor;
  }

  _createClass(Kelas, [{
    key: "nama",
    get: function get() {
      return this._nama;
    },
    set: function set(strnama) {
      this._nama = strnama;
    } // get student(){
    //     return this._student
    // }
    // set student(strstudent){
    //     this._student = strstudent
    // }

  }, {
    key: "level",
    get: function get() {
      return this._level;
    },
    set: function set(level) {
      this._level = level;
    }
  }, {
    key: "instructor",
    get: function get() {
      return this._instructor;
    },
    set: function set(instructor) {
      this._instructor = instructor;
    }
  }, {
    key: "students",
    get: function get() {
      return this._students;
    }
  }, {
    key: "addStudents",
    value: function addStudents(objkelas) {
      this._students.push(objkelas);
    }
  }]);

  return Kelas;
}();

var _default = Kelas;
exports["default"] = _default;