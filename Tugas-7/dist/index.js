"use strict";

var _status = _interopRequireDefault(require("./libs/status"));

var _tags = _interopRequireDefault(require("./libs/tags"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// 
var sanber = new _status["default"]("Sanbercode");
sanber.createKelas("Laravel", "beginner", "abduh");
sanber.createKelas("React", "beginner", "abdul");
var namelist = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"];
namelist.map(function (tag, index) {
  var newStud = new _tags["default"](tag);
  var kelas = sanber.student[index % 2].nama;
  sanber.insertStudent(kelas, newStud);
});
sanber.student.forEach(function (post) {
  console.log(post);
});