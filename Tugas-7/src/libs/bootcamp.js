import kelas from "./postingan"

class bootcamp{
    constructor(nama){
        this._nama = nama
        this._student = []
        
    }
    get nama(){
        return this._nama
    }
    set nama(strnama){
        this._nama = strnama
    }

    get student(){
    return this._student
    }
    set student(strstudent){
    this._student = strstudent
    }   
    createKelas(nama,  level, instructor){
        let newKelas = new kelas(nama,  level, instructor)
        this._student.push(newKelas)
    }

    insertStudent(nama, objstudent){
        let carinama = this._student.find((post) => post.nama === nama)
        carinama.addStudent(objstudent)
    }
    
}

export default bootcamp;