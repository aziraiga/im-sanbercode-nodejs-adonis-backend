console.log("Soal 1")
var biodata=[
    ["0001", "Roman Alamsyah", "Bandar Lampung 21/05/1989", "Membaca"],
     ["0002", "Dika Sembiring", "Medan 10/10/1992", "Bermain Gitar"],
     ["0003", "Winona", "Ambon 25/12/1965", "Memasak"],
      ["0004", "Bintang Senjaya", "Martapura 6/4/1970", "Berkebun"]
]

function datahandling(data){
   for (var i = 0; i < data.length; i++){
        console.log("id trainer :" , data[i][0])
        console.log("Nama Lengkap :" , data[i][1])
        console.log("TTL :" , data[i][2])
        console.log("Hobi :" , data[i][3])
        console.log("");
   }
}

datahandling(biodata);

console.log("Soal 2")

function balikKata(kata){
    var output = "";
    var panjangkata = kata.length;

    for (var a=panjangkata-1; a>=0; a--) {
        output+=kata[a];
    }
   return output;
}


    console.log(balikKata("SanberCode"));
//Output: edoCrebnaS

console.log(balikKata("racecar"));
//Output: racecar
console.log(balikKata("kasur rusak"));
//Output: kasur rusak

console.log(balikKata("haji ijah"));
//Output: haji ijah

console.log(balikKata("I am Sanbers"));
//Output: srebnaS ma 
