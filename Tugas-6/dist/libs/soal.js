"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sapa = exports.filterData = exports.data = exports.checkScore = void 0;

var sapa = function sapa(nama) {
  return "Halo selamat pagi, ".concat(nama);
};

exports.sapa = sapa;

var data = function data(name, domisili, umur) {
  return {
    name: name,
    domisili: domisili,
    umur: umur
  };
};

exports.data = data;

var checkScore = function checkScore(nama1, kelas, nilai) {
  return [nama1];
};

exports.checkScore = checkScore;
var data1 = [{
  name: "Ahmad",
  kelas1: "adonis"
}, {
  name: "Regi",
  kelas1: "laravel"
}, {
  name: "Bondra",
  kelas1: "adonis"
}, {
  name: "Iqbal",
  kelas1: "vuejs"
}, {
  name: "Putri",
  kelas1: "Laravel"
}];

var filterData = function filterData(kelas1) {
  for (var i = 0; i < data1.length; i++) {
    return data1.filter(function (el) {
      return el['kelas1'].toLowerCase().includes(kelas1.toLowerCase());
    });
  }
};

exports.filterData = filterData;