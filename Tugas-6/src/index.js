import {sapa, data, checkScore, filterData} from './libs/soal'

const myArgs = process.argv.slice(2);
const command = myArgs[0]

switch (command) {
    case 'sapa':
        let nama = myArgs[1]
        console.log(sapa(nama));

        break;
        case 'data':
            const biodata = myArgs.slice(1);
            let [name,domisili,umur] = biodata

            console.log(data(name, domisili, umur))

            break;

        case 'checkScore':
            const cekskor = myArgs.slice(1);
            let [nama1, kelas,  nilai] = cekskor
            cekskor.shift()
            console.log(checkScore(nama1 , kelas , nilai));
          
            break;

        case 'filterData' :
            let kelas1 = myArgs[1];
            console.log(filterData(kelas1));

            break;

            default:
            break;
        
}