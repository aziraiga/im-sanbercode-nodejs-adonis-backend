import karyawan from "./karyawan";
import fs from "fs";
import "core-js/stable";
import fsPromises from "fs/promises";

const path = "data.json";

class bootcamp{
    static register(input){
        let [name, password, role] = input.split(",");
        fs.readFile(path, (err, data)=>{
            if(err){
                console.log(err);
            }

            let existingData = JSON.parse(data);
            let Karyawan = new karyawan(name, password, role);
            existingData.push(Karyawan);
            fs.writeFile(path, JSON.stringify(existingData), (err)=>{
                if (err){
                    console.log(err);
                }else {
                    console.log("Berhasil Register");
                }
            })
        })
    }

    static login(input){
        let [name, password] = input.split(",");

        fsPromises
        .readFile(path)
        .then((data)=>{
            let pekerja = JSON.parse(data);

            let indexEmp = pekerja.findIndex((emp) => emp._name == name);
            if (indexEmp == -1){
                console.log("data tidak ditemukan");
            } else {
                let employee = pekerja[indexEmp]

                if(employee._password == password){
                    employee._islogin = true;  

                    pekerja.splice(indexEmp, 1, employee);
                    return fsPromises.writeFile(path, JSON.stringify(pekerja));
                }else {
                    console.log("Password salah");
                }
            }
        })
        .then(()=>{
            console.log("Berhasil Login")
        })
        .catch((err)=>{
            console.log(err)
        })
    }
    static addSiswa(input){
            let [name, trainer] = input.split(",")

            fsPromises.readFile(path).then((data)=>{
                let siswa = JSON.parse(data);
                let siswabaru = siswa.findIndex((sis) => sis.name == name)


            })
}}

export default bootcamp;